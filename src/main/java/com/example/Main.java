/*
 * Copyright 2002-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import spark.Spark;

import javax.sql.DataSource;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@SpringBootApplication
public class Main {
    static private final String url = "https://53a907b6011d6275b0ab22df5cb741f5:553e1ed1ab55b3032b6854d753d8e3b4@kellifrank.myshopify.com";
    static private final String client_id = "hxMpTMrhR0rdUTfhuaIDbG3N2VO82G02";
    static private final String redirect_uri = "http://localhost:5000/callback";
    static private final String state = "";
    static private final String authToken = "5OtjwgBqfIMt7t2R4KDWp_QKI7KMCOYAtXglPSvI";

    static private final String vendUrl = "https://secure.vendhq.com/connect?response_type=code&client_id=" + client_id + "&redirect_uri=" + redirect_uri + "&state=" + state;
    static private final String domain_prefix = "kellifrank";
    @Value("${spring.datasource.url}")
    private String dbUrl;

    private static Connection connection;

    static {
        try {
            connection = getConnection();
        } catch (URISyntaxException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private DataSource dataSource;

    private static String parseVend(HttpResponse<JsonNode> response) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("DELETE FROM heroku_d4c605d757261c3.product where SKU > 0");
        statement.execute();

        JSONArray products = (JSONArray) response.getBody().getObject().get("products");

        products.forEach(product -> {
            JSONObject prod = (JSONObject) product;
            String handle = (String) prod.get("handle");
            handle = handle.replaceAll("-", "");
            String variant = (String) prod.get("variant_option_one_value");

            int SKU;
            try {
                SKU = Integer.parseInt((String) prod.get("sku"));
                double price = Double.parseDouble(prod.get("price") + "");

                JSONArray inv = (JSONArray) prod.get("inventory");
                JSONObject info = (JSONObject) inv.get(0);
                int count = Integer.parseInt((String) info.get("count"));

                try {
                    addToTable(SKU, handle, count, price, variant);
                } catch (SQLException | URISyntaxException e) {
                    e.printStackTrace();
                }
            } catch (NumberFormatException e) {
            }

        });
        return "";
    }

    private static ArrayList<ArrayList<String>> parseShopify(HttpResponse<JsonNode> response) throws SQLException, URISyntaxException {
        JSONArray products = (JSONArray) response.getBody().getObject().get("products");

        products.forEach(product -> {
            JSONObject prod = (JSONObject) product;
            String handle = (String) prod.get("handle");
            handle = handle.replaceAll("-", "");

            prod = (JSONObject) ((JSONArray) prod.get("variants")).get(0);

            int SKU = Integer.parseInt((String) prod.get("sku"));
            double price = Double.parseDouble((String) prod.get("price"));
            String variant = (String) prod.get("title");

            int inv = (int) prod.get("inventory_quantity");

            try {

                PreparedStatement statement = connection.prepareStatement("SELECT quantity FROM heroku_d4c605d757261c3.product where `SKU` = ? and `NAME` = ?");
                statement.setInt(1, SKU);
                statement.setString(2, handle);
                ResultSet rs = statement.executeQuery();
                if (rs.next()) {
                    int existingQuant = rs.getInt(1);

                    updateRow(SKU, existingQuant + inv, handle);
                }
//                addToTable(SKU, handle, inv, price, variant);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        });

        return getDBContents();

    }

    static ArrayList<ArrayList<String>> getDBContents() throws SQLException, URISyntaxException {
        return getDBContents("SELECT * FROM heroku_d4c605d757261c3.product");
    }

    static ArrayList<ArrayList<String>> getDBContents(String query) throws SQLException, URISyntaxException {
        connection = getConnection();
        ArrayList<ArrayList<String>> info = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet rs = statement.executeQuery();

        ResultSetMetaData rsmd = null;
        while (rs.next()) {
            ArrayList<String> line = new ArrayList<>();
            if (rsmd == null) {
                rsmd = rs.getMetaData();
//                for (int i = 0; i < rsmd.getColumnCount(); i++) {
//                    line.add(rsmd.getColumnName(i + 1)); //headers
//                }
////                rs.next();
//                info.add(line);
            }

            line = new ArrayList<>();
            for (int i = 0; i < rsmd.getColumnCount(); i++) {
                String item = rs.getObject(i + 1).toString();
                if (i == 3) {
                    item = "$" + item;
                    if (item.indexOf(".") == item.length() - 2) {
                        item += "0";
                    }
                }
                line.add(item);
            }
            info.add(line);
        }

        connection.close();
        return info;
    }

    private static void updateRow(int sku, int quant, String name) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("UPDATE `heroku_d4c605d757261c3`.`product` SET `quantity` = ? WHERE (`SKU` = ? and `NAME` = ?)");

        statement.setInt(1, quant);
        statement.setInt(2, sku);
        statement.setString(3, name);
        statement.execute();
    }

//    static String getInfo(HttpResponse<JsonNode> response) throws UnirestException, SQLException, URISyntaxException {
//        String handleInfo = "Handles: ";
//        String skuInfo = "SKUs: ";
//        String quantityInfo = "quantities: ";
//        String priceInfo = "prices: ";
//
//        JSONArray data = (JSONArray) response.getBody().getObject().get("products");
//
//        for (int i = 0; i < data.length(); i++) {
//            System.out.println(data.get(i));
//            JSONObject obj = (JSONObject) data.get(i);
//            handleInfo += obj.get("handle") + ", ";
//            String handle = (String) obj.get("handle");
//
//            try {
//                obj = (JSONObject) ((JSONArray) obj.get("variants")).get(0);
//            } catch (JSONException e) {
//
//            }
//
//            int quant;
//            try {
//                quant = (int) obj.get("inventory_quantity");
//            } catch (JSONException e) {
//                quant = Integer.parseInt((String) ((JSONObject) ((JSONArray) obj.get("inventory")).get(0)).get("count"));
//            }
//
//            skuInfo += obj.get("sku") + ", ";
//            quantityInfo += quant + ", ";
//            priceInfo += obj.get("price") + ", ";
//
//            addToTable(Integer.parseInt((String) obj.get("sku")),
//                    handle,
//                    quant,
//                    (Double) obj.get("price"),
//                    );
//
//        }
//
//
//        return handleInfo + "\n" + skuInfo + "\n" + quantityInfo + "\n" + priceInfo;
//    }

    private static Connection getConnection() throws URISyntaxException, SQLException {
        URI dbUri = new URI(System.getenv("CLEARDB_DATABASE_URL"));

        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        String dbUrl = "jdbc:mysql://" + dbUri.getHost() + dbUri.getPath();

        System.out.println(username + " , " + password + " , " + dbUrl);

        return DriverManager.getConnection(dbUrl, username, password);
    }


    private static void addToTable(int SKU, String name, int quantity, double price, String variant) throws SQLException, URISyntaxException {
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(
                    "INSERT INTO `heroku_d4c605d757261c3`.`product` (`SKU`, `name`, `quantity`, `price`, `variant`) VALUES (?,?,?,?,?)"
            );
            statement.setInt(1, SKU);
            statement.setString(2, name);
            statement.setInt(3, quantity);
            statement.setDouble(4, price);
            statement.setString(5, variant);

            statement.execute();
        } catch (MySQLIntegrityConstraintViolationException e) {

        }
    }

    //    static String getVendInfo(int id) throws UnirestException {
//        HttpResponse<JsonNode> response = Unirest.get("https://" + domain_prefix + ".vendhq.com/api/products/" + id)
//                .header("cache-control", "no-cache")
//                .header("Authorization", "Bearer 5OtjwgBqfIMt7t2R4KDWp_QKI7KMCOYAtXglPSvI")
//                .asJson();
//
//        JSONArray data = (JSONArray) response.getBody().getObject().get("products");
//
//        String handleInfo = "Handles: ";
//        String skuInfo = "SKUs: ";
//        String quantityInfo = "quantities: ";
//        String priceInfo = "prices: ";
//
//
//        for (Object obj : data) {
//            handleInfo += ((JSONObject) obj).get("handle") + "\t";
//        }
//
//        return handleInfo;
//    }
    static int getIntFromEnv(String envar, int defaultVal) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get(envar) != null) {
            return Integer.parseInt(processBuilder.environment().get(envar));
        }
        return defaultVal;
    }

    // Enables CORS on requests. This method is an initialization method and should be called once.
    private static void enableCORS(final String origin, final String methods, final String headers) {

        Spark.options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        Spark.before((request, response) -> {
            response.header("Access-Control-Allow-Origin", origin);
            response.header("Access-Control-Request-Method", methods);
            response.header("Access-Control-Allow-Headers", headers);
            // Note: this may or may not be necessary in your particular application
            response.type("application/json");
        });
    }


    public static void main(String[] args) throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        SpringApplication.run(Main.class, args);
    }

    @RequestMapping(value = "/api/sync", method = POST)
    String refresh(Map<String, Object> model) throws URISyntaxException, UnirestException, SQLException {
        model.put("row", refreshDBInfo());
        return "info";
    }

    @RequestMapping("/api/products/{productId}")
    String getSpecificProduct(Map<String, Object> model, @PathVariable String productId) throws SQLException, URISyntaxException {
        model.put("row", getDBContents("SELECT * FROM heroku_d4c605d757261c3.product WHERE SKU = " + productId));
        return "info";
    }

    @RequestMapping("/api/products")
    String routeAll(Map<String, Object> model) throws URISyntaxException, UnirestException, SQLException {
        model.put("row", refreshDBInfo());
        return "info";
    }

    @RequestMapping("/")
    String index(Map<String, Object> model) throws UnirestException, SQLException, URISyntaxException {
        refreshDBInfo();
        return "index";
    }

    private static ArrayList<ArrayList<String>> refreshDBInfo() throws UnirestException, SQLException, URISyntaxException {
        ArrayList<ArrayList<String>> info = new ArrayList<>();
        connection = getConnection();

        //        vend:
        HttpResponse<JsonNode> vendResponse = Unirest.get("https://" + domain_prefix + ".vendhq.com/api/products")
                .header("cache-control", "no-cache")
                .header("Authorization", "Bearer 5OtjwgBqfIMt7t2R4KDWp_QKI7KMCOYAtXglPSvI")
                .asJson();


//        shopify:
        HttpResponse<JsonNode> shopifyResponse = Unirest.get(url + "/admin/products.json")
                .header("cache-control", "no-cache")
                .asJson();

        parseVend(vendResponse);
        info.addAll(parseShopify(shopifyResponse));

        connection.close();

        return info;
    }


    @Bean
    public DataSource dataSource() throws SQLException {
        if (dbUrl == null || dbUrl.isEmpty()) {
            return new HikariDataSource();
        } else {
            HikariConfig config = new HikariConfig();
            config.setJdbcUrl(dbUrl);
            return new HikariDataSource(config);
        }
    }

}
