# Kelli Frank Stitch Labs Project
A site to access all items in my stores across Vend and Shopify in a single location.

## Access
* Access the site at <href>https://sleepy-crag-23064.herokuapp.com/</href>
* Site has all the recommended endpoints
